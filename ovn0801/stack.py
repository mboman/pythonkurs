#!/usr/bin/python3
# coding: utf-8

"""Övning 8.1

* Skapa en klass Stack.

* Konstruktorn ska skapa en tom stack.

* Metoden push(value) ska lägga till ett element.

* Metoden pop() ska ta bort och returnera senaste elementet.

* Om man försöker göra pop på en tom stack så ska ett exception av typen
  TomStack genereras.

* Det ska gå att skriva ut stackinnehållet med print.

"""

